"""
This is a small script that cuts PDFs into
individual files and rename them according to
a specific name string inside

Functions:

main()
"""

import os
import shutil
from tika import parser
from PyPDF2 import PdfWriter, PdfReader

# Directories names and paths
PDF_ORIGINAL_DIR = "pdfOriginal/"
PDF_RENAME_DIR = "pdfRename/"
TMP_DIR = PDF_RENAME_DIR + "fichierTemporaire/"


def main():
    """
    A single main function for this script!
    """

    # create the needed directories
    os.makedirs(PDF_RENAME_DIR, exist_ok=True)
    os.makedirs(TMP_DIR, exist_ok=True)

    # look for all files in the Original dir
    for files in os.listdir(PDF_ORIGINAL_DIR):

        # open the found PDF file
        with open(PDF_ORIGINAL_DIR + files, "rb") as file:
            inputpdf = PdfReader(file, strict=False)

            # for each page of the input document, create
            # a file in the tmp dir
            for i in range(len(inputpdf.pages)):
                output = PdfWriter()
                output.add_page(inputpdf.pages[i])

                # add padding of zeros to the page number
                pagenum = str(i).rjust(3, "0")

                with open(f'{TMP_DIR}{pagenum}', "wb") as output_stream:
                    output.write(output_stream)

            # close the original PDF file
            file.close()

        # for all the individual page files
        for split_files in sorted(os.listdir(TMP_DIR)):
            # get the raw data for each file
            raw = parser.from_file(TMP_DIR + split_files)
            # raw data in text format
            txt = raw['content']
            # raw text as an array
            tab_txt_line = txt.splitlines()

            # when it doesn't exist, create the output dir
            os.makedirs(PDF_RENAME_DIR + tab_txt_line[29]+"/", exist_ok=True)

            # then, rename the tmp pdf with the new found names,
            # read as text from the file data
            # ! This part is extermely depandant to the format of the
            # original pdf and will not work with other input file.
            os.rename(TMP_DIR + split_files,
                      PDF_RENAME_DIR + tab_txt_line[29]+"/" +
                      str(split_files) + " - " +
                      tab_txt_line[26].replace(".", "") + ".pdf")

    # clear the temp dir
    os.rmdir(TMP_DIR)

    # Creates a zip archive of the output dir
    shutil.make_archive("attestations", "zip", PDF_RENAME_DIR)

    # clear the renamed files dir
    shutil.rmtree(PDF_RENAME_DIR)


if __name__ == "__main__":
    main()
